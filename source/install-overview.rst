########
Overview
########

.. contents:: Table of Contents

----------

This guide provides installation and uninstallation procedures for the AMDGPU
graphics and compute stack.

.. note:: The rest of this document will refer to Radeon™ Software for Linux®
     as AMDGPU.

---------

Stack Variants
**************

There are two major stack variants available for installation:

- **Pro**: recommended for use with **Radeon Pro** graphics products.
- **All-Open**: recommended for use with consumer products.

+--------------------+------------------------------------------------+
| Installing Option  | Components                                     |
+====================+================================================+
| All-Open Stack     | * Base kernel drivers                          |
|                    | * Base accelerated graphics drivers            |
|                    | * Mesa OpenGL                                  |
|                    | * Mesa multimedia                              |
+--------------------+------------------------------------------------+
| Pro Stack          | * Base kernel drivers                          |
|                    | * Base accelerated graphics drivers            |
|                    | * Mesa multimedia                              |
|                    | * Pro OpenGL                                   |
|                    | * Pro OpenCL                                   |
|                    |    - PAL OpenCL stack (supports Vega 10 and    |
|                    |      later products)                           |
|                    |    - Legacy OpenCL stack (supports legacy      |
|                    |      products older than Vega 10)              |
|                    | * Pro Vulkan                                   |
+--------------------+------------------------------------------------+

You can install a combination of stack components using the ``amdgpu-install``
script, as listed in the following:

* Base kernel and accelerated graphics drivers + Pro OpenGL + Pro Vulkan
* Base kernel and accelerated graphics drivers + Pro OpenGL + Pro Vulkan +
  Pro OpenCL
* Base kernel and accelerated graphics drivers + Pro OpenCL
* Base kernel drivers (**no** accelerated graphics drivers) + Pro OpenCL
  (headless mode)


.. note::
   - When installing the All-Open stack using ``amdgpu-install`` script, every
     component from the All-Open stack will be installed. There is no
     supported way to install arbitrary combinations of these components when
     using ``amdgpu-install`` but you can use the system package manager to do
     so.

   - 32-bit libraries are automatically installed with Pro OpenGL component when
     installation is performed using ``amdgpu-install`` script.
